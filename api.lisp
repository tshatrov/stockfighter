
(in-package :stockfighter)

(defvar *default-api*)

(defvar *default-order*)

(defclass api ()
  ((key :initarg :key :reader api-key)))

(defun login (api-key)
  (setf *default-api* (make-instance 'api :key api-key)))

(define-condition http-error (error)
  ((code :reader http-error-code :initarg :code))
  (:report (lambda (condition stream)
             (format stream "HTTP error: ~S" (http-error-code condition)))))

(defmethod request ((api api) url &rest args)
  (multiple-value-bind (response status)
      (apply 'webgunk:http-request url
             :additional-headers `(("X-Starfighter-Authorization" . ,(api-key api)))
             args)
    (if (= status 200)
        (jsown:parse response)
        (error 'http-error :code status))))

(defgeneric get-request-args (obj)
  (:method (obj)))

(defclass entry-point ()
  ((base-url :initarg :url :reader base-url)))

(defmethod get-url ((obj entry-point))
  (base-url obj))

(defmethod get-request-args ((obj entry-point))
  (call-next-method))

(defmethod call ((obj entry-point) &key (api *default-api*))
  (apply 'request api (get-url obj) (get-request-args obj)))

(defun make-call (cls &rest extra-args)
  (let ((args (when *default-order* (get-args *default-order*))))
    (loop for (key value . rest) on extra-args by #'cddr
       do (setf (getf args key) value))
    (call (apply 'make-instance cls :allow-other-keys t args))))

(defclass post-mixin ()
  ((post-data :initform nil :accessor post-data)))
  
(defmethod get-request-args ((obj post-mixin))
  (append `(:method :post :content ,(jsown:to-json (post-data obj)) :content-type "application/json")
          (call-next-method)))

(defclass delete-mixin () ())

(defmethod get-request-args ((obj delete-mixin))
  (append `(:method :delete) (call-next-method)))

(defclass heartbeat (entry-point)
  ((base-url :initform "https://api.stockfighter.io/ob/api/heartbeat")))


(defclass venue-heartbeat (entry-point)
  ((base-url :initform "https://api.stockfighter.io/ob/api/venues/~a/heartbeat")
   (venue :initarg :venue :reader venue) ;; "TESTEX"
   ))
  
(defmethod get-url ((obj venue-heartbeat))
  (format nil (base-url obj) (venue obj)))


(defclass venue-stocks (entry-point)
  ((base-url :initform "https://api.stockfighter.io/ob/api/venues/~a/stocks")
   (venue :initarg :venue :reader venue)
   ))
  
(defmethod get-url ((obj venue-stocks))
  (format nil (base-url obj) (venue obj)))


(defclass orderbook (entry-point)
  ((base-url :initform "https://api.stockfighter.io/ob/api/venues/~a/stocks/~a")
   (venue :initarg :venue :reader venue)
   (stock :initarg :stock :reader stock)
   ))
  
(defmethod get-url ((obj orderbook))
  (format nil (base-url obj) (venue obj) (stock obj)))

(defclass order (entry-point post-mixin)
  ((base-url :initform "https://api.stockfighter.io/ob/api/venues/~a/stocks/~a/orders")
   (venue :initarg :venue :reader venue)
   (stock :initarg :stock :reader stock)
   (account :initarg :account :reader account)
   ))

(defmethod get-url ((obj order))
  (format nil (base-url obj) (venue obj) (stock obj)))

(defmethod place-order ((order order) price qty direction &optional (order-type "limit"))
  (setf (post-data order)
        (jsown:new-js
          ("account" (account order))
          ("venue" (venue order))
          ("stock" (stock order))
          ("price" price)
          ("qty" qty)
          ("direction" direction)
          ("orderType" order-type)))
  (call order))

(defun place-order* (price qty direction &optional (order-type "limit"))
  (place-order *default-order* price qty direction order-type))

(defmethod get-args ((order order))
  `(:venue ,(venue order) :stock ,(stock order) :account ,(account order)))

(defun make-order (account venue stock &key (default t) (class 'plog))
  (let ((order (make-instance class :account account :venue venue :stock stock)))
    (when default
      (setf *default-order* order))
    order))

(defmethod cancel-order ((order order) order-id)
  (let ((*default-order* order))
    (make-call 'order-cancel :id order-id)))

(defun cancel-order* (order-id)
  (cancel-order *default-order* order-id))

(defmethod read-order ((order order) order-id)
  (let ((*default-order* order))
    (make-call 'order-status :id order-id)))

(defun read-order* (order-id)
  (read-order *default-order* order-id))

(defclass stock-quote (entry-point)
  ((base-url :initform "https://api.stockfighter.io/ob/api/venues/~a/stocks/~a/quote")
   (venue :initarg :venue :reader venue)
   (stock :initarg :stock :reader stock)
   ))
  
(defmethod get-url ((obj stock-quote))
  (format nil (base-url obj) (venue obj) (stock obj)))

(defclass order-status (entry-point)
  ((base-url :initform "https://api.stockfighter.io/ob/api/venues/~a/stocks/~a/orders/~a")
   (venue :initarg :venue :reader venue)
   (stock :initarg :stock :reader stock)
   (order-id :initarg :id :reader order-id)
   ))

(defmethod get-url ((obj order-status))
  (format nil (base-url obj) (venue obj) (stock obj) (order-id obj)))

(defclass order-cancel (order-status delete-mixin)
  ((base-url :initform "https://api.stockfighter.io/ob/api/venues/~a/stocks/~a/orders/~a")
   (venue :initarg :venue :reader venue)
   (stock :initarg :stock :reader stock)
   (order-id :initarg :id :reader order-id)
   ))

(defclass orders (entry-point)
  ((base-url :initform "https://api.stockfighter.io/ob/api/venues/~a/accounts/~a/orders")
   (venue :initarg :venue :reader venue)
   (account :initarg :account :reader account)
   ))

(defmethod get-url ((obj orders))
  (format nil (base-url obj) (venue obj) (account obj)))

