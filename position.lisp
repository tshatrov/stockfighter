
(in-package :stockfighter)

(defclass plog (order)
  ((shares :initform 0 :accessor shares)
   (cash :initform 0 :accessor cash)
   (last-price :initform nil :accessor last-price)
   (active-orders :initform nil :accessor active-orders)
   ))

(defmethod avg-price ((obj plog))
  (cond ((= (shares obj) 0) nil)
        (t (round (- (cash obj)) (shares obj)))))

(defmethod assets ((obj plog))
  (+ (cash obj) (* (or (last-price obj) 0) (shares obj))))

(defmethod position-str ((obj plog))
  (format nil "Shares: ~a; Cash: ~a; Assets: ~a; Avg: ~a"
          (shares obj) (cash obj)
          (assets obj)
          (or (avg-price obj) "--")))

(defgeneric finalize (obj info)
  (:method (obj info) ))

(defmethod finalize ((obj plog) order-info)
  "Record info of a cancelled/finished order into plog"
  (let ((fills (val-safe order-info "fills"))
        (direction (val order-info "direction"))
        (order-id (val order-info "id")))
    (loop
       for fill in fills
       for price = (val fill "price")
       for qty = (val fill "qty")
       summing qty into dshares
       summing (* price qty) into dcash
       finally (cond ((string= direction "buy")
                      (incf (shares obj) dshares)
                      (decf (cash obj) dcash))
                     ((string= direction "sell")
                      (decf (shares obj) dshares)
                      (incf (cash obj) dcash)))
         )
    (setf (active-orders obj) (remove order-id (active-orders obj)))
    ))

(defmethod place-order ((plog plog) price qty direction &optional (order-type "limit"))
  (declare (ignore order-type))
  (let* ((response (call-next-method))
         (order-id (val response "id")))
    (push order-id (active-orders plog))
    response))
    
(defmethod call ((obj order-cancel) &key)
  (let ((result (call-next-method)))
    (when *default-order*
      (finalize *default-order* result))
    result))
  
(defmethod update-info ((plog plog))
  (loop with flag = nil
     for order-id in (active-orders plog)
     for info = (read-order plog order-id)
     for status = (val info "open")
     unless status
     do (finalize plog info) (setf flag t)
     finally (return flag)))
       
(defmethod cancel-all ((plog plog))
  (loop
     for order-id in (active-orders plog)
     for info = (read-order plog order-id)
     for status = (val info "open")
     if status do (cancel-order plog order-id)
     else do (finalize plog info)))
