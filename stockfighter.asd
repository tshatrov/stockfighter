;;;; stockfighter.asd

(asdf:defsystem #:stockfighter
  :description "API for www.stockfighter.io"
  :author "Timofei Shatrov <timofei.shatrov@gmail.com>"
  :license "MIT"
  :serial t
  :depends-on (:webgunk
               :drakma
               :cl-ppcre)
  :components ((:file "package")
               (:file "api")
               (:file "position")
               (:file "bot")))

